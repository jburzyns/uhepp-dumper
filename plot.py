#!/usr/bin/env python3

"""
Script to convert TH1 objects in to uhepp plots
author: Jackson Burzynski (jackson.carl.burzynski@cern.ch)
"""

import sys
import os
import argparse
import re
import matplotlib.pyplot as plt
import uhepp
import ROOT
import json
import numpy as np

color_dict = {"313399": "#17BEFF",
              "313415": "#E377C2"}

legend_dict = {"313399": "$m_a, c\\tau$ = [55, 100]",
               "313415": "$m_a, c\\tau$ = [16, 100]"}

class PlotItem:

    def __init__(self,sample,hist,bins=None):
        self.sample = sample
        self.hist = hist
        self.bins = bins

        self.color = color_dict[sample]
        self.legend = legend_dict[sample]

def parse_file(file):

    DSID = re.findall(r'.(\d{6})', file)
    return DSID[0]

def get_args():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'inputFiles',
        help='ROOT file(s) to visualize',
        nargs='*'
    )

    args = parser.parse_args()
    if not args.inputFiles:
        parser.print_usage()
        sys.exit('ERROR: need to provide a path to valid ROOT file to plot')
    return args

def get_hists(sample, file, unit_normalize=True):
    """Get the collection of histograms from the input file

    The return object is a list of dictionaries with (algorithm:variable)
    key value pairs
    """

    hists = {}

    file.cd()
    dir_keys = ROOT.gDirectory.GetListOfKeys()
    for dir_key in dir_keys:
        # go back to top level, then cd to cut dir
        file.cd()

        if "Hist" not in dir_key.GetName():
            continue

        file.cd(dir_key.GetName())
        algorithm = dir_key.GetName()

        var_keys = ROOT.gDirectory.GetListOfKeys()
        hists[algorithm] = {}
        for var_key in var_keys:
            variable = var_key.GetName()
            th1 = file.Get(f"{algorithm}/{variable}")
            if unit_normalize and th1.Integral():
                th1.Scale(1.0/th1.Integral())
            uh1, bins = uhepp.from_th1(th1, include_bins=True)
            plot = PlotItem(sample,uh1,bins)
            hists[algorithm][variable] = plot

    return hists

def test(uh_yield, var):

    hist = uhepp.UHeppHist("$\mu$", map(lambda x: round(x,2), uh_yield[0].bins))

    hist.yields = {
        "313399": uh_yield[0].hist,
        "313415": uh_yield[1].hist
    }
    hist.variable = "Reduced mass"
    hist.unit = "GeV"
    hist.brand = "ATLAS"
    hist.brand_label = "Internal"
    hist.subtext = "$n_{\mathrm{trk}} > 2$"
    hist.unit_in_brackets = True
    hist.y_label = "Fraction of vertices"

    #hist.rebin_edges = list(range(0,25,1))

    signal_1 = uhepp.StackItem(["313399"], uh_yield[0].legend, color=uh_yield[0].color)
    signal_2 = uhepp.StackItem(["313415"], uh_yield[1].legend, color=uh_yield[1].color)
    mc_stack1 = uhepp.Stack([signal_1], bartype='step')
    mc_stack2 = uhepp.Stack([signal_2], bartype='step')
    hist.stacks.append(mc_stack1)
    hist.stacks.append(mc_stack2)

    hist.show()
    hist.render(f"{var}.pdf")
    hist.push(130)
    hist.to_json("test.json")


def main():

    args = get_args()
    inputFiles = args.inputFiles

    TFiles  = {}
    hist_db = []
    for file in inputFiles:
       sample = parse_file(file)
       TFiles[sample] = ROOT.TFile(file)
       hists = get_hists(sample, TFiles[sample])
       hist_db.append(hists)

    for var in hist_db[0]["DVHists_ntrk3"]:
        test([x["DVHists_ntrk3"][var] for x in hist_db], var)



if __name__ == '__main__':
  main()
